Domaci
Napisati JavaScript funkciju koja prima niz numeričkih brojeva i vraća
drugi najmanji i drugi najveći broj iz niza.
Koristiti sort, push i join metodu za izradu zadatka
Parametar: [13,105,75,1,344,6,5]
Rezultat: 5,105